package com.mochizuki.tetris;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Disposable;

public class Bricks {

	static public abstract class AbsBrick extends Actor implements Disposable {

		public final static int CLOCKWISE = 0;
		public final static int ANTICLOCKWISE = 1;

		protected int[][][] m_Matrix;
		// protected int[][] m_currentMatrix;

		protected int m_currentState;
		private float m_piceWidth;
		private float m_piceHeight;
		private Texture m_Texture;
		private TextureRegion m_piceTexture;

		private int m_CoordLine;// 当前方块的位置座标，以左上角为基准
		private int m_CoordColumn;
		protected int m_PiceColor;

		@Override
		public void dispose() {
			// TODO Auto-generated method stub
			m_Texture.dispose();
			// m_Texture=null;
		}

		@Override
		public void draw(SpriteBatch batch, float parentAlpha) {
			// TODO Auto-generated method stub
			for (int y = 3; y >= 0; y--) {
				for (int x = 0; x < 4; x++) {
					if (m_Matrix[m_currentState][y][x] > 0)
						batch.draw(m_piceTexture, getX() + x * m_piceWidth, getY()
								+ (3 - y) * m_piceHeight, m_piceWidth,
								m_piceHeight);
				}
			}
			// super.draw(batch, parentAlpha);
		}

		public AbsBrick() {
			initMatrix();
		}

		public void setPiceSize(float picewidth, float piceheight) {
			m_piceHeight = piceheight;
			m_piceWidth = picewidth;

		}

		/**
		 * 取得当前的方块矩阵
		 * 
		 * @return
		 */
		public int[][] getCurrentMatrix() {
			return m_Matrix[m_currentState];

		}

		/**
		 * 取得下一个旋转后的矩阵数据
		 * 
		 * @param direction
		 * @return
		 */
		public int[][] getNextMatrix(int direction) {
			int state = 0;
			switch (direction) {
			case CLOCKWISE:
				if (m_currentState == 3)
					state = 0;
				else
					state = m_currentState + 1;
				break;
			case ANTICLOCKWISE:
				if (m_currentState == 0)
					state = 3;
				else
					state = m_currentState - 1;
				break;
			}
			return m_Matrix[state];

		}

		public void initMatrix() {
			m_PiceColor = (int) (Math.round(Math.random() * 6)) + 1;
			m_Texture = null;
//			m_Texture = new Texture(Gdx.files.internal(String.format(
//					"bricks/%1$d.png", m_PiceColor)));
			m_Texture=new Texture(Gdx.files.internal("bricks/cube.png"));
			m_piceTexture=new TextureRegion(m_Texture,18*(m_PiceColor-1),0,17,17);
			// Log.i(this.getClass().getSimpleName(), "color=" + m_PiceColor);
			m_Matrix = new int[4][4][4];
			m_currentState = 0;
			setWidth(m_piceWidth * 4f);
			setHeight(m_piceHeight * 4f);
			// m_piceWidth = getWidth() / 4f;
			// m_piceHeight = getHeight() / 4f;

			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					for (int k = 0; k < 4; k++)
						m_Matrix[i][j][k] = 0;
				}

			}
		}

		public int mostLeft(int[][] matrix) {
			for (int col = 0; col < 4; col++) {
				for (int line = 0; line < 4; line++) {
					if (matrix[line][col] > 0)
						return col;
				}
			}
			return 3;

		}

		public int mostRight(int[][] matrix) {
			for (int col = 3; col >= 0; col--) {
				for (int line = 0; line < 4; line++) {
					if (matrix[line][col] > 0)
						return col;
				}
			}
			return 0;

		}

		public int mostBottom(int[][] matrix) {
			for (int line = 3; line >= 0; line--) {
				for (int col = 0; col < 4; col++) {
					if (matrix[line][col] > 0)
						return line;
				}
			}
			return 0;

		}

		/**
		 * 旋转一次(90度)
		 * 
		 * @param direction
		 *            旋转方向
		 */
		public void Rotation(int direction) {
			switch (direction) {
			case CLOCKWISE:
				if (m_currentState == 3)
					m_currentState = 0;
				else
					m_currentState++;
				break;
			case ANTICLOCKWISE:
				if (m_currentState == 0)
					m_currentState = 3;
				else
					m_currentState--;
				break;
			}
		}

		public int getCoordLine() {
			return m_CoordLine;
		}

		public void setCoordLine(int CoordLine) {
			this.m_CoordLine = CoordLine;
		}

		public int getCoordColumn() {
			return m_CoordColumn;
		}

		public void setCoordColumn(int CoordColumn) {
			this.m_CoordColumn = CoordColumn;
		}

		public int getCurrentState() {
			return m_currentState;
		}

		public void setCurrentState(int state) {
			m_currentState = state;
		}

		public int getCurrentColor() {
			return m_PiceColor;
		}

		public void setCurrentColor(int color) {
			m_PiceColor = color;
		}

	}

	static public class LongBrick extends AbsBrick {

		@Override
		public void initMatrix() {
			// TODO Auto-generated method stub
			super.initMatrix();
			for (int i = 0; i < 4; i++) {
				m_Matrix[0][1][i] = m_PiceColor;
				m_Matrix[1][i][2] = m_PiceColor;
				m_Matrix[2][1][i] = m_PiceColor;
				m_Matrix[3][i][2] = m_PiceColor;
			}
		}

	}

	static public class TshapeBrick extends AbsBrick {

		@Override
		public void initMatrix() {
			// TODO Auto-generated method stub
			super.initMatrix();
			for (int j = 0; j < 3; j++) {
				m_Matrix[0][1][j] = m_PiceColor;
				m_Matrix[1][j + 1][2] = m_PiceColor;
				m_Matrix[2][3][j] = m_PiceColor;
				m_Matrix[3][j + 1][0] = m_PiceColor;
			}
			m_Matrix[0][2][1] = m_PiceColor;
			m_Matrix[1][2][1] = m_PiceColor;
			m_Matrix[2][2][1] = m_PiceColor;
			m_Matrix[3][2][1] = m_PiceColor;
		}

	}

	static public class SquareBrick extends AbsBrick {

		@Override
		public void initMatrix() {
			// TODO Auto-generated method stub
			super.initMatrix();
			for (int i = 1; i < 3; i++) {
				for (int j = 1; j < 3; j++) {
					m_Matrix[0][i][j] = m_PiceColor;
					m_Matrix[1][i][j] = m_PiceColor;
					m_Matrix[2][i][j] = m_PiceColor;
					m_Matrix[3][i][j] = m_PiceColor;
				}
			}
		}

	}

	static public class LshapeBrick extends AbsBrick {

		@Override
		public void initMatrix() {
			// TODO Auto-generated method stub
			super.initMatrix();
			for (int i = 0; i < 3; i++) {
				m_Matrix[0][i][1] = m_PiceColor;
				m_Matrix[1][1][i] = m_PiceColor;
				m_Matrix[2][i][1] = m_PiceColor;
				m_Matrix[3][1][i] = m_PiceColor;

			}
			m_Matrix[0][2][2] = m_PiceColor;

			m_Matrix[1][2][0] = m_PiceColor;

			m_Matrix[2][0][0] = m_PiceColor;

			m_Matrix[3][0][2] = m_PiceColor;
		}

	}

	static public class JshapeBrick extends AbsBrick {

		@Override
		public void initMatrix() {
			// TODO Auto-generated method stub
			super.initMatrix();
			for (int i = 0; i < 3; i++) {
				m_Matrix[0][i][1] = m_PiceColor;
				m_Matrix[1][1][i] = m_PiceColor;
				m_Matrix[2][i][1] = m_PiceColor;
				m_Matrix[3][1][i] = m_PiceColor;

			}
			m_Matrix[0][2][0] = m_PiceColor;

			m_Matrix[1][0][0] = m_PiceColor;

			m_Matrix[2][0][2] = m_PiceColor;

			m_Matrix[3][2][2] = m_PiceColor;
		}

	}

	static public class ZshapeBrick extends AbsBrick {

		@Override
		public void initMatrix() {
			// TODO Auto-generated method stub
			super.initMatrix();
			for (int i = 0; i < 2; i++) {
				m_Matrix[0][1][i] = m_PiceColor;
				m_Matrix[0][2][i + 1] = m_PiceColor;

				m_Matrix[1][i][1] = m_PiceColor;
				m_Matrix[1][i + 1][0] = m_PiceColor;

				m_Matrix[2][1][i] = m_PiceColor;
				m_Matrix[2][2][i + 1] = m_PiceColor;

				m_Matrix[3][i][1] = m_PiceColor;
				m_Matrix[3][i + 1][0] = m_PiceColor;

			}
		}

	}

	static public class SshapeBrick extends AbsBrick {

		@Override
		public void initMatrix() {
			// TODO Auto-generated method stub
			super.initMatrix();
			for (int i = 0; i < 2; i++) {
				m_Matrix[0][1][i + 1] = m_PiceColor;
				m_Matrix[0][2][i] = m_PiceColor;

				m_Matrix[1][i][0] = m_PiceColor;
				m_Matrix[1][i + 1][1] = m_PiceColor;

				m_Matrix[2][1][i + 1] = m_PiceColor;
				m_Matrix[2][2][i] = m_PiceColor;

				m_Matrix[3][i][0] = m_PiceColor;
				m_Matrix[3][i + 1][1] = m_PiceColor;

			}
		}

	}
}
